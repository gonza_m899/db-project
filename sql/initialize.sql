-- Services
INSERT INTO `NORTV_DB`.`Servicio` (`codigoServicio`, `NombreServicio`, `DescripcionServicio`, `PrecioServicio`) VALUES (1, 'Estandar', '60 canales', 400.0);
INSERT INTO `NORTV_DB`.`Servicio` (`codigoServicio`, `NombreServicio`, `DescripcionServicio`, `PrecioServicio`) VALUES (2, 'Plus', 'Estandar + 4 canales de peliculas y 2 canales HD', 600.0);
INSERT INTO `NORTV_DB`.`Servicio` (`codigoServicio`, `NombreServicio`, `DescripcionServicio`, `PrecioServicio`) VALUES (3, 'Premium', 'Estandar + 6 canales de peliculas y 4 canales HD', 699.0);

-- Zones
INSERT INTO `NORTV_DB`.`Zona` (`idZona`, `NombreZona`) VALUES (1, 'Zona 1');
INSERT INTO `NORTV_DB`.`Zona` (`idZona`, `NombreZona`) VALUES (2, 'Zona 2');
INSERT INTO `NORTV_DB`.`Zona` (`idZona`, `NombreZona`) VALUES (3, 'Zona 3');
INSERT INTO `NORTV_DB`.`Zona` (`idZona`, `NombreZona`) VALUES (4, 'Zona 4');

-- Locations
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (1, 'Resistencia', 1);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (2, 'Barranqueras', 1);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (3, 'Fontana', 2);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (4, 'Tirol', 2);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (5, 'Margarita Belén', 3);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (6, 'Colonia Benítez', 3);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (7, 'Cote Lai', 4);
INSERT INTO `NORTV_DB`.`Localidad` (`idLocalidad`, `NombreLocalidad`, `ZonaLocalidad`) VALUES (8, 'Charadai', 4);

-- Locations / Zones
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (1, 1);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (2, 1);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (3, 2);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (4, 2);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (5, 3);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (6, 3);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (7, 4);
INSERT INTO `NORTV_DB`.`ZonaLocalidad` (`localidad`, `zona`) VALUES (8, 4);

-- Clients
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (1, 12345678, 'CORRECTO', 'Test1', 'Teste', 'Calle Falsa 3421', '36247856512', 1, 1);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (2, 12712412, 'SERVICIO_LIMITADO', 'Test2', 'Tester', 'Calle Falsa 2345', '34531231212', 2, 1);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (3, 11241226, 'CORRECTO', 'Test3', 'Tester', 'Calle Falsa 1234', '36242383651', 3, 8);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (4, 12411212, 'SERVICIO_LIMITADO', 'Test4', 'Tester', 'Calle Falsa 4321', '36212412523', 1, 3);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (5, 12412312, 'SIN_SERVICIO', 'Test5', 'Tester', 'Calle Falsa 2367', '36241412421', 2, 4);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (6, 12756543, 'SERVICIO_LIMITADO', 'Test6', 'Tester', 'Calle Falsa 3467', '36242383651', 3, 4);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (7, 12789456, 'CORRECTO', 'Test7', 'Tester', 'Calle Falsa 2365', '36242383651', 2, 6);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (8, 12346332, 'SIN_SERVICIO', 'Test8', 'Tester', 'Calle Falsa 2398', '36242383651', 1, 7);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (9, 12789456, 'SERVICIO_LIMITADO', 'Test9', 'Tester', 'Calle Falsa 2387', '36242383651', 2, 3);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (10, 23455657, 'SERVICIO_LIMITADO', 'Test10', 'Tester', 'Calle Falsa 1265', '36242383651', 3, 1);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (11, 37456621, 'NUEVO_PENDIENTE', 'Test11', 'Tester', 'Calle Falsa 1265', '36242383651', 3, 8);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (12, 12789456, 'SIN_SERVICIO_REPORTADO', 'Test12', 'Tester', 'Calle Falsa 1298', '36242383651', 1, 7);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (13, 23525323, 'SERVICIO_LIMITADO', 'Test13', 'Tester', 'Calle Falsa 1245', '36242383651', 2, 6);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (14, 34534534, 'NUEVO_PENDIENTE', 'Test14', 'Tester', 'Calle Falsa 1278', '36242383651', 3, 5);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (15, 12789456, 'SERVICIO_LIMITADO', 'Test15', 'Tester', 'Calle Falsa 1272', '36242383651', 2, 6);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (16, 12512131, 'SIN_SERVICIO', 'Test16', 'Tester', 'Calle Falsa 12572', '36242383651', 1, 4);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (17, 34568762, 'SERVICIO_LIMITADO', 'Test17', 'Tester', 'Calle Falsa 1262', '36242383651', 2, 8);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (18, 39876453, 'CORRECTO', 'Moni', 'Test18', 'Calle Falsa 5312', '36242383651', 3, 3);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (19, 12412412, 'SERVICIO_LIMITADO', 'Test19', 'Tester', 'Calle Falsa 2442', '36242383651', 2, 8);
INSERT INTO `NORTV_DB`.`Cliente` (`nroCliente`, `DNICliente`, `EstadoCliente`, `NombreCliente`, `ApellidoCliente`, `DireccionCliente`, `TelefonoCliente`, `ServicioCliente`, `LocalidadCliente`) VALUES (20, 23231422, 'NUEVO_PENDIENTE', 'Test20', 'Tester', 'Calle Falsa 1773', '36242383651', 3, 3);

-- Technicians
INSERT INTO `NORTV_DB`.`Tecnico` (`idTecnico`, `DNITecnico`, `NombreTecnico`, `ApellidoTecnico`, `DireccionTecnico`, `TelefonoTecnico`, `LocalidadTecnico`) VALUES (1, 31890341, 'Tecnico1', 'Test', 'Calle Falsa 5332', '3121231231', 1);
INSERT INTO `NORTV_DB`.`Tecnico` (`idTecnico`, `DNITecnico`, `NombreTecnico`, `ApellidoTecnico`, `DireccionTecnico`, `TelefonoTecnico`, `LocalidadTecnico`) VALUES (2, 32151234, 'Tecnico1', 'Test', 'Calle Falsa 125', '3621253223', 1);
INSERT INTO `NORTV_DB`.`Tecnico` (`idTecnico`, `DNITecnico`, `NombreTecnico`, `ApellidoTecnico`, `DireccionTecnico`, `TelefonoTecnico`, `LocalidadTecnico`) VALUES (3, 32156752, 'Tecnico3', 'Test', 'Calle Falsa 1253', '3622125122', 2);
INSERT INTO `NORTV_DB`.`Tecnico` (`idTecnico`, `DNITecnico`, `NombreTecnico`, `ApellidoTecnico`, `DireccionTecnico`, `TelefonoTecnico`, `LocalidadTecnico`) VALUES (4, 35325211, 'Tecnico4', 'Test', 'Calle Falsa 4212', '3664233423', 2);

-- Orders
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (1, CURRENT_DATE() , 'NUEVO', 2, 1);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (2, CURRENT_DATE() , 'NUEVO', 4, 2);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (3, CURRENT_DATE() , 'NUEVO', 5, 2);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (4, CURRENT_DATE() , 'NUEVO', 6, 2);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (5, CURRENT_DATE() , 'NUEVO', 8, 4);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (6, CURRENT_DATE() , 'NUEVO', 9, 2);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (7, CURRENT_DATE() , 'NUEVO', 10, 1);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (8, CURRENT_DATE() , 'NUEVO', 11, 4);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (9, CURRENT_DATE() , 'NUEVO', 12, 4);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (10, CURRENT_DATE() , 'NUEVO', 13, 3);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (11, CURRENT_DATE() , 'NUEVO', 14, 3);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (12, CURRENT_DATE() , 'NUEVO', 15, 3);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (13, CURRENT_DATE() , 'NUEVO', 16, 2);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (14, CURRENT_DATE() , 'NUEVO', 17, 4);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (15, CURRENT_DATE() , 'NUEVO', 19, 4);
INSERT INTO `NORTV_DB`.`Pedido` (`idPedido`, `FechaPedido`, `EstadoPedido`, `ClientePedido`, `ZonaPedido`) VALUES (16, CURRENT_DATE() , 'NUEVO', 20, 2);

-- Complains
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (1, 'CONEXION_LIMITADA', 'El cable se corta cada 5 minutos. Por favor solucionenlo', 1);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (2, 'CONEXION_LIMITADA', 'El cable se corta cada 10 minutos. Por favor solucionenlo', 2);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (3, 'SIN_CONEXION', 'Estoy sin señal hace 4 hs!! Solucionenlo por favor!', 3);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (4, 'CONEXION_LIMITADA', 'El cable se corta cada 5 minutos. Por favor solucionenlo', 4);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (5, 'SIN_CONEXION', 'Estoy sin señal hace 2 hs!! Solucionenlo por favor!', 5);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (6, 'CONEXION_LIMITADA', 'El cable se corta cada 15 minutos. Por favor solucionenlo', 6);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (7, 'CONEXION_LIMITADA', 'El cable se corta cada 30 minutos. Por favor solucionenlo', 7);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (8, 'SIN_CONEXION', 'Estoy sin señal hace 6 hs!! Solucionenlo por favor!', 9);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (9, 'CONEXION_LIMITADA', 'El cable se corta cada 15 minutos. Por favor solucionenlo', 10);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (10, 'CONEXION_LIMITADA', 'El cable se corta cada 5 minutos. Por favor solucionenlo', 12);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (11, 'SIN_CONEXION', 'Estoy sin señal hace 9 hs!! Solucionenlo por favor!', 13);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (12, 'CONEXION_LIMITADA', 'El cable se corta cada 30 minutos. Por favor solucionenlo', 14);
INSERT INTO `NORTV_DB`.`Reclamo` (`idReclamo`, `TipoReclamo`, `DescripcionReclamo`, `PedidoReclamo`) VALUES (13, 'CONEXION_LIMITADA', 'El cable se corta cada 5 minutos. Por favor solucionenlo', 15);

-- Contracts
INSERT INTO `NORTV_DB`.`Contratacion` (`nroContratacion`, `FormaPago`, `FechaInstalacion`, `PedidoContratacion`) VALUES (1, 'DEBITO', CURRENT_DATE() , 8);
INSERT INTO `NORTV_DB`.`Contratacion` (`nroContratacion`, `FormaPago`, `FechaInstalacion`, `PedidoContratacion`) VALUES (2, 'DEBITO', STR_TO_DATE('2016-11-28','%Y-%m-%d'), 11);
INSERT INTO `NORTV_DB`.`Contratacion` (`nroContratacion`, `FormaPago`, `FechaInstalacion`, `PedidoContratacion`) VALUES (3, 'DEBITO', CURRENT_DATE() , 16);