package com.mujim.dbproject.data.dao;

import com.mujim.dbproject.model.Service;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
public interface ServiceDAO {

    List<Service> getServices();
}
