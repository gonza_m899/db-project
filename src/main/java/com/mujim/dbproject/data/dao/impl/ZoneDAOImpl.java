package com.mujim.dbproject.data.dao.impl;

import com.mujim.dbproject.data.ConnectionManager;
import com.mujim.dbproject.data.dao.ZoneDAO;
import com.mujim.dbproject.model.Zone;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@Component
public class ZoneDAOImpl implements ZoneDAO {

    @Override
    public List<Zone> getZones() {
        List<Zone> zones = new ArrayList<>();

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM Zona");
            while (resultSet.next()) {
                Zone zone = new Zone();
                zone.setZoneId(resultSet.getInt("idZona"));
                zone.setZoneName(resultSet.getString("NombreZona"));
                zones.add(zone);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            try {
                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return zones;
    }
}
