package com.mujim.dbproject.data.dao.impl;

import com.mujim.dbproject.data.ConnectionManager;
import com.mujim.dbproject.data.dao.OrderDAO;
import com.mujim.dbproject.model.*;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@Component
public class OrderDAOImpl implements OrderDAO {

    @Override
    public List<Complain> getComplaints() {
        List<Complain> complains = new ArrayList<>();
        try {
            Connection connection = ConnectionManager.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT r.idReclamo, p.EstadoPedido, p.FechaPedido, r.TipoReclamo, r.DescripcionReclamo, c.nroCliente, c.NombreCliente, c.ApellidoCliente, t.idTecnico, t.NombreTecnico, t.ApellidoTecnico, z.* FROM Pedido p" +
                    " INNER JOIN Reclamo r ON p.idPedido = r.PedidoReclamo" +
                    " INNER JOIN Cliente c ON p.ClientePedido = c.nroCliente" +
                    " INNER JOIN Zona z ON p.ZonaPedido = z.idZona"+
                    " LEFT JOIN Tecnico t ON p.TecnicoPedido = t.idTecnico");
            while (resultSet.next()) {
                Complain complain = new Complain();
                complain.setComplainId(resultSet.getInt("idReclamo"));
                complain.setType(ComplainType.valueOf(resultSet.getString("TipoReclamo")));
                complain.setDescription(resultSet.getString("DescripcionReclamo"));
                complain.setOrderDate(resultSet.getDate("FechaPedido"));
                complain.setStatus(OrderStatus.valueOf(resultSet.getString("EstadoPedido")));
                complain.setClient(parseClientData(resultSet));
                complain.setTechnician(parseTechnicianData(resultSet));
                complain.setZone(parseZoneData(resultSet));
                complains.add(complain);
            }
            statement.close();
            resultSet.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return complains;
    }

    private Client parseClientData(ResultSet resultSet) throws SQLException {
        Client client = new Client();
        client.setClientNumber(resultSet.getInt("nroCliente"));
        client.setFirstName(resultSet.getString("NombreCliente"));
        client.setLastName(resultSet.getString("ApellidoCliente"));

        return client;
    }

    @Override
    public List<RoadMapOrder> getOrdersForRoadMap(int zoneId) {
        List<RoadMapOrder> roadMapOrders = new ArrayList<>();
        String query = "SELECT c.nroCliente, c.NombreCliente, c.ApellidoCliente, c.DireccionCliente, z.NombreZona, c.EstadoCliente, s.NombreServicio " +
                "FROM NORTV_DB.Pedido p " +
                "INNER JOIN NORTV_DB.Zona z ON p.ZonaPedido = z.idZona " +
                "INNER JOIN NORTV_DB.Cliente c ON p.ClientePedido = c.nroCliente " +
                "INNER JOIN NORTV_DB.Servicio s ON c.ServicioCliente = s.codigoServicio " +
                "LEFT JOIN NORTV_DB.Contratacion ct ON p.idPedido = ct.PedidoContratacion " +
                "WHERE z.idZona = ? AND (c.EstadoCliente IN ('SERVICIO_LIMITADO', 'SIN_SERVICIO', 'SIN_SERVICIO_REPORTADO') OR (ct.nroContratacion IS NOT NULL AND c.EstadoCliente = 'NUEVO_PENDIENTE' AND ct.FechaInstalacion = DATE(NOW())))";
        try {
            Connection connection = ConnectionManager.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, zoneId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                RoadMapOrder order = new RoadMapOrder();
                order.setClientNumber(resultSet.getInt("nroCliente"));
                order.setClientFirstName(resultSet.getString("NombreCliente"));
                order.setClientLastName(resultSet.getString("ApellidoCliente"));
                order.setClientAddress(resultSet.getString("DireccionCliente"));
                order.setZoneName(resultSet.getString("NombreZona"));
                order.setClientStatus(ClientStatus.valueOf(resultSet.getString("EstadoCliente")));
                order.setServiceName(resultSet.getString("NombreServicio"));
                roadMapOrders.add(order);
            }
            statement.close();
            resultSet.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roadMapOrders;
    }

    private Technician parseTechnicianData(ResultSet resultSet) throws SQLException {
        Technician technician = null;
        if (resultSet.getInt("idTecnico") != 0) {
            technician = new Technician();
            technician.setTechnicianId(resultSet.getInt("idTecnico"));
            technician.setFirstName(resultSet.getString("NombreTecnico"));
            technician.setLastName(resultSet.getString("ApellidoTecnico"));
        }
        return technician;
    }

    private Zone parseZoneData(ResultSet resultSet) throws SQLException {
        Zone zone = new Zone();
        zone.setZoneId(resultSet.getInt("idZona"));
        zone.setZoneName(resultSet.getString("NombreZona"));
        return zone;
    }
}
