package com.mujim.dbproject.data.dao;

import com.mujim.dbproject.model.Complain;
import com.mujim.dbproject.model.RoadMapOrder;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
public interface OrderDAO {

    List<RoadMapOrder> getOrdersForRoadMap(int zoneId);

    List<Complain> getComplaints();
}
