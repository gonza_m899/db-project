package com.mujim.dbproject.data.dao;

import com.mujim.dbproject.model.Zone;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
public interface ZoneDAO {

    List<Zone> getZones();
}
