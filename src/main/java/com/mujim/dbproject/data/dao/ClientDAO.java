package com.mujim.dbproject.data.dao;

import com.mujim.dbproject.model.Client;
import com.mujim.dbproject.model.Contract;
import com.mujim.dbproject.model.request.CreateClientRequest;
import com.mujim.dbproject.model.response.CreateClientResponse;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
public interface ClientDAO {

    CreateClientResponse createClient(CreateClientRequest createClientRequest);

    List<Client> getClients();
}
