package com.mujim.dbproject.data.dao.impl;

import com.mujim.dbproject.data.ConnectionManager;
import com.mujim.dbproject.data.dao.LocationDAO;
import com.mujim.dbproject.model.Location;
import com.mujim.dbproject.model.Service;
import com.mujim.dbproject.model.Zone;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@Component
public class LocationDAOImpl implements LocationDAO {

    @Override
    public List<Location> getLocations() {
        List<Location> locations = new ArrayList<>();

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT l.idLocalidad, l.NombreLocalidad, z.* " +
                                                         "FROM Localidad l INNER JOIN Zona z ON l.ZonaLocalidad = z.idZona ");
            while (resultSet.next()) {
                Location location = new Location();
                location.setLocationId(resultSet.getInt("idLocalidad"));
                location.setLocationName(resultSet.getString("NombreLocalidad"));
                location.setZone(parseZoneData(resultSet));
                locations.add(location);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return locations;
    }

    private Zone parseZoneData(ResultSet resultSet) throws SQLException {
        Zone zone = new Zone();
        zone.setZoneId(resultSet.getInt("idZona"));
        zone.setZoneName(resultSet.getString("NombreZona"));
        return zone;
    }
}
