package com.mujim.dbproject.data.dao.impl;

import com.mujim.dbproject.data.dao.ClientDAO;
import com.mujim.dbproject.data.ConnectionManager;
import com.mujim.dbproject.model.*;
import com.mujim.dbproject.model.request.CreateClientRequest;
import com.mujim.dbproject.model.response.CreateClientResponse;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@Component
public class ClientDAOImpl implements ClientDAO {


    @Override
    public CreateClientResponse createClient(CreateClientRequest createClientRequest) {
        int affectedRows, clientNumber, orderId;

        String insertClientSQL = "INSERT INTO Cliente (DNICliente, EstadoCliente, NombreCliente, ApellidoCliente, DireccionCliente, TelefonoCliente, ServicioCliente, LocalidadCliente)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        String insertOrderSQL = "INSERT INTO Pedido (FechaPedido, EstadoPedido, ClientePedido, ZonaPedido) VALUES (?, ?, ?, ?)";
        String insertContractSQL = "INSERT INTO Contratacion (FormaPago, FechaInstalacion, PedidoContratacion) VALUES (?, ?, ?)";

        Connection connection = null;
        PreparedStatement createClientStatement= null;
        PreparedStatement createOrderStatement = null;
        PreparedStatement createContractStatement = null;
        try {
            connection = ConnectionManager.getConnection();
            connection.setAutoCommit(false);
            createClientStatement = connection.prepareStatement(insertClientSQL, Statement.RETURN_GENERATED_KEYS);
            createClientStatement.setInt(1, createClientRequest.getClientId());
            createClientStatement.setString(2, ClientStatus.NUEVO_PENDIENTE.toString());
            createClientStatement.setString(3, createClientRequest.getFirstName());
            createClientStatement.setString(4, createClientRequest.getLastName());
            createClientStatement.setString(5, createClientRequest.getAddress());
            createClientStatement.setString(6, createClientRequest.getPhone());
            createClientStatement.setInt(7, (int) createClientRequest.getService().getServiceId());
            createClientStatement.setInt(8, (int) createClientRequest.getLocation().getLocationId());
            affectedRows = createClientStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Failed to create user, no rows affected.");
            }

            try (ResultSet generatedKeys = createClientStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    clientNumber = generatedKeys.getInt(1);
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

            createOrderStatement = connection.prepareStatement(insertOrderSQL, Statement.RETURN_GENERATED_KEYS);
            createOrderStatement.setDate(1, new Date(System.currentTimeMillis()));
            createOrderStatement.setString(2, OrderStatus.NUEVO.toString());
            createOrderStatement.setInt(3, clientNumber);
            createOrderStatement.setInt(4, (int) createClientRequest.getLocation().getZone().getZoneId());
            affectedRows = createOrderStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Failed to create user, no rows affected.");
            }

            try (ResultSet generatedKeys = createOrderStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    orderId = generatedKeys.getInt(1);
                }
                else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }

            createContractStatement = connection.prepareStatement(insertContractSQL, Statement.RETURN_GENERATED_KEYS);
            createContractStatement.setString(1, createClientRequest.getPaymentType().toString());
            createContractStatement.setDate(2, new Date(createClientRequest.getInstallationDate().getTime()));
            createContractStatement.setInt(3, orderId);
            affectedRows = createContractStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Failed to create user, no rows affected.");
            }

            connection.commit();

            return new CreateClientResponse(true, "Cliente #" + clientNumber + " creado exitosamente!");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                createClientStatement.close();
                createOrderStatement.close();
                createContractStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new CreateClientResponse(false, "No se pudo crear el cliente debido a un error. Verifique los datos de entrada.");
    }

    @Override
    public List<Client> getClients() {
        List<Client> clients = new ArrayList<>();
        try {
            Connection connection = ConnectionManager.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT c.nroCliente, c.DNICliente, c.EstadoCliente, c.NombreCliente, c.ApellidoCliente," +
                                                                "c.DireccionCliente, c.TelefonoCliente, s.*, l.*" +
                                                            " FROM Cliente c" +
                                                            " INNER JOIN Servicio s ON c.ServicioCliente = s.codigoServicio" +
                                                            " INNER JOIN Localidad l ON c.LocalidadCliente = l.idLocalidad");
            while (resultSet.next()) {
                Client client = new Client();
                client.setClientNumber(resultSet.getInt("nroCliente"));
                client.setStatus(ClientStatus.valueOf(resultSet.getString("EstadoCliente")));
                client.setService(parseServiceData(resultSet));
                client.setId(resultSet.getInt("DNICliente"));
                client.setFirstName(resultSet.getString("NombreCliente"));
                client.setLastName(resultSet.getString("ApellidoCliente"));
                client.setAddress(resultSet.getString("DireccionCliente"));
                client.setPhone(resultSet.getString("TelefonoCliente"));
                client.setLocation(parseLocationData(resultSet));
                clients.add(client);
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clients;
    }

    private Service parseServiceData(ResultSet resultSet) throws SQLException {
        Service service = new Service();
        service.setServiceId(resultSet.getInt("codigoServicio"));
        service.setName(resultSet.getString("NombreServicio"));
        service.setDescription(resultSet.getString("DescripcionServicio"));
        service.setPrice(resultSet.getDouble("PrecioServicio"));
        return service;
    }

    private Location parseLocationData(ResultSet resultSet) throws SQLException {
        Location location = new Location();
        location.setLocationId(resultSet.getInt("idLocalidad"));
        location.setLocationName(resultSet.getString("NombreLocalidad"));
        return location;
    }
}
