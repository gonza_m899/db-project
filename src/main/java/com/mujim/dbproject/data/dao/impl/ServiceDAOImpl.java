package com.mujim.dbproject.data.dao.impl;

import com.mujim.dbproject.data.ConnectionManager;
import com.mujim.dbproject.data.dao.ServiceDAO;
import com.mujim.dbproject.model.Service;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@Component
public class ServiceDAOImpl implements ServiceDAO {

    @Override
    public List<Service> getServices() {
        List<Service> services = new ArrayList<>();

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = ConnectionManager.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM Servicio");
            while (resultSet.next()) {
                Service service = new Service();
                service.setServiceId(resultSet.getInt("codigoServicio"));
                service.setName(resultSet.getString("NombreServicio"));
                service.setDescription(resultSet.getString("DescripcionServicio"));
                service.setPrice(resultSet.getDouble("PrecioServicio"));
                services.add(service);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            try {
                resultSet.close();
                statement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return services;
    }
}
