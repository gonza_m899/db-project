package com.mujim.dbproject.controller;

import com.mujim.dbproject.data.dao.ServiceDAO;
import com.mujim.dbproject.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@RestController
public class ServiceController {

    @Autowired
    private ServiceDAO serviceDAO;

    @RequestMapping(path = "/api/services", method = RequestMethod.GET)
    public ResponseEntity<List<Service>> getServices() {
        return ResponseEntity.ok(serviceDAO.getServices());
    }
}
