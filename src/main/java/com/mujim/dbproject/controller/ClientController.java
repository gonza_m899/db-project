package com.mujim.dbproject.controller;

import com.mujim.dbproject.data.dao.ClientDAO;
import com.mujim.dbproject.model.Client;
import com.mujim.dbproject.model.request.CreateClientRequest;
import com.mujim.dbproject.model.response.CreateClientResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@RestController
@RequestMapping("/api/clients")
public class ClientController {

    @Autowired
    private ClientDAO clientDAO;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Client>> getClients() {
        return ResponseEntity.ok(clientDAO.getClients());
    }

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public ResponseEntity<CreateClientResponse> createClient(@RequestBody CreateClientRequest createClientRequest) {
        CreateClientResponse response = clientDAO.createClient(createClientRequest);
        if (response.isSuccessfull()) {
            return ResponseEntity.ok(response);
        }
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
