package com.mujim.dbproject.controller;

import com.mujim.dbproject.data.dao.OrderDAO;
import com.mujim.dbproject.model.Complain;
import com.mujim.dbproject.model.RoadMapOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@RestController
@RequestMapping("/api/orders")
public class OrderController {

    @Autowired
    private OrderDAO orderDAO;

    @RequestMapping(path = "/complains", method = RequestMethod.GET)
    public ResponseEntity<List<Complain>> getComplains() {
        return ResponseEntity.ok(orderDAO.getComplaints());
    }

    @RequestMapping(path = "/roadmap", method = RequestMethod.GET)
    public ResponseEntity<List<RoadMapOrder>> getOrderForRoadMap(@RequestParam("zoneId") int zoneId) {
        return ResponseEntity.ok(orderDAO.getOrdersForRoadMap(zoneId));
    }
}
