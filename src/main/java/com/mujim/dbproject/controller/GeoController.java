package com.mujim.dbproject.controller;

import com.mujim.dbproject.data.dao.LocationDAO;
import com.mujim.dbproject.data.dao.ZoneDAO;
import com.mujim.dbproject.model.Location;
import com.mujim.dbproject.model.Zone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by gonzalo on 23/11/16.
 */
@RestController
@RequestMapping("/api/geo")
public class GeoController {

    @Autowired
    private LocationDAO locationDAO;

    @Autowired
    private ZoneDAO zoneDAO;

    @RequestMapping(path = "/locations", method = RequestMethod.GET)
    public ResponseEntity<List<Location>> getLocations() {
        return ResponseEntity.ok(locationDAO.getLocations());
    }

    @RequestMapping(path = "/zones", method = RequestMethod.GET)
    public ResponseEntity<List<Zone>> getZones() {
        return ResponseEntity.ok(zoneDAO.getZones());
    }
}
