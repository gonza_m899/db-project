package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Location {

    private long locationId;
    private String locationName;
    private Zone zone;

    public Location() {
    }

    public Location(long locationId, String locationName, Zone zone) {
        this.locationId = locationId;
        this.locationName = locationName;
        this.zone = zone;
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(long locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }
}
