package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contract extends Order {

    private long contractId;
    private PaymentType paymentType;
    private Date installationDate;

    public Contract() {
    }

    public Contract(long contractId, PaymentType paymentType, Date installationDate) {
        this.contractId = contractId;
        this.paymentType = paymentType;
        this.installationDate = installationDate;
    }

    public Contract(long orderId, Date orderDate, OrderStatus status, Date resolutionDate, Technician technician, Client client, Zone zone, long contractId, PaymentType paymentType, Date installationDate) {
        super(orderId, orderDate, status, resolutionDate, technician, client, zone);
        this.contractId = contractId;
        this.paymentType = paymentType;
        this.installationDate = installationDate;
    }

    public long getContractId() {
        return contractId;
    }

    public void setContractId(long contractId) {
        this.contractId = contractId;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }
}
