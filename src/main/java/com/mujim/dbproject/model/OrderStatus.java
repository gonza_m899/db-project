package com.mujim.dbproject.model;

/**
 * Created by gonzalo on 25/11/16.
 */
public enum OrderStatus {
    NUEVO,
    PENDIENTE,
    SIN_RESOLVER,
    RESUELTO
}
