package com.mujim.dbproject.model;

/**
 * Created by gonzalo on 25/11/16.
 */
public enum PaymentType {
    EFECTIVO,
    CREDITO,
    DEBITO
}
