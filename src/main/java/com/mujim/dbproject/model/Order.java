package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Order {

    private long orderId;
    private Date orderDate;
    private OrderStatus status;
    private Date resolutionDate;
    private Technician technician;
    private Client client;
    private Zone zone;

    public Order() {
    }

    public Order(long orderId, Date orderDate, OrderStatus status, Date resolutionDate, Technician technician, Client client, Zone zone) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.status = status;
        this.resolutionDate = resolutionDate;
        this.technician = technician;
        this.client = client;
        this.zone = zone;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Date getResolutionDate() {
        return resolutionDate;
    }

    public void setResolutionDate(Date resolutionDate) {
        this.resolutionDate = resolutionDate;
    }

    public Technician getTechnician() {
        return technician;
    }

    public void setTechnician(Technician technician) {
        this.technician = technician;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }
}
