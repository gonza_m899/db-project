package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Complain extends Order {

    private long complainId;
    private ComplainType type;
    private String description;

    public Complain() {
    }

    public Complain(long complainId, ComplainType type, String description) {
        this.complainId = complainId;
        this.type = type;
        this.description = description;
    }

    public Complain(long orderId, Date orderDate, OrderStatus status, Date resolutionDate, Technician technician, Client client, Zone zone, long complainId, ComplainType type, String description) {
        super(orderId, orderDate, status, resolutionDate, technician, client, zone);
        this.complainId = complainId;
        this.type = type;
        this.description = description;
    }

    public long getComplainId() {
        return complainId;
    }

    public void setComplainId(long complainId) {
        this.complainId = complainId;
    }

    public ComplainType getType() {
        return type;
    }

    public void setType(ComplainType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
