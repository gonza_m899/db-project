package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Technician extends Person {

    private long technicianId;

    public Technician() {
    }

    public Technician(long technicianId) {
        this.technicianId = technicianId;
    }

    public Technician(int id, String firstName, String lastName, String address, String phone, Location location, long technicianId) {
        super(id, firstName, lastName, address, phone, location);
        this.technicianId = technicianId;
    }

    public long getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(long technicianId) {
        this.technicianId = technicianId;
    }
}
