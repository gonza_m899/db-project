package com.mujim.dbproject.model;

/**
 * Created by gonzalo on 25/11/16.
 */
public class RoadMapOrder {
    private long clientNumber;
    private String clientFirstName;
    private String clientLastName;
    private String clientAddress;
    private String zoneName;
    private ClientStatus clientStatus;
    private String serviceName;

    public RoadMapOrder() {
    }

    public RoadMapOrder(long clientNumber, String clientFirstName, String clientLastName, String clientAddress, String zoneName, ClientStatus clientStatus, String serviceName) {
        this.clientNumber = clientNumber;
        this.clientFirstName = clientFirstName;
        this.clientLastName = clientLastName;
        this.clientAddress = clientAddress;
        this.zoneName = zoneName;
        this.clientStatus = clientStatus;
        this.serviceName = serviceName;
    }

    public long getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(long clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getClientFirstName() {
        return clientFirstName;
    }

    public void setClientFirstName(String clientFirstName) {
        this.clientFirstName = clientFirstName;
    }

    public String getClientLastName() {
        return clientLastName;
    }

    public void setClientLastName(String clientLastName) {
        this.clientLastName = clientLastName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public ClientStatus getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(ClientStatus clientStatus) {
        this.clientStatus = clientStatus;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
