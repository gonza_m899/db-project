package com.mujim.dbproject.model.request;

import com.mujim.dbproject.model.Location;
import com.mujim.dbproject.model.PaymentType;
import com.mujim.dbproject.model.Service;

import java.util.Date;

/**
 * Created by gonzalo on 25/11/16.
 */
public class CreateClientRequest {

    private int clientId;
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private Location location;
    private Service service;
    private PaymentType paymentType;
    private Date installationDate;

    public CreateClientRequest() {
    }

    public CreateClientRequest(int clientId, String firstName, String lastName, String address, String phone, Location location, Service service, PaymentType paymentType, Date installationDate) {
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.location = location;
        this.service = service;
        this.paymentType = paymentType;
        this.installationDate = installationDate;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }
}
