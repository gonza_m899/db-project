package com.mujim.dbproject.model;

/**
 * Created by gonzalo on 25/11/16.
 */
public enum ComplainType {
    CONEXION_LIMITADA,
    SIN_CONEXION
}
