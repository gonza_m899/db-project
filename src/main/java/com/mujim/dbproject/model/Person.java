package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person {

    private int id;
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private Location location;

    public Person() {
    }

    public Person(int id, String firstName, String lastName, String address, String phone, Location location) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
