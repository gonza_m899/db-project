package com.mujim.dbproject.model.response;

/**
 * Created by gonzalo on 25/11/16.
 */
public class CreateClientResponse {

    private boolean successfull;
    private String message;

    public CreateClientResponse() {
    }

    public CreateClientResponse(boolean successfull, String message) {
        this.successfull = successfull;
        this.message = message;
    }

    public boolean isSuccessfull() {
        return successfull;
    }

    public void setSuccessfull(boolean successfull) {
        this.successfull = successfull;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
