package com.mujim.dbproject.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by gonzalo on 23/11/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Client extends Person {

    private long clientNumber;
    private ClientStatus status;
    private Service service;

    public Client() {
    }

    public Client(long clientNumber, ClientStatus status, Service service) {
        this.clientNumber = clientNumber;
        this.status = status;
        this.service = service;
    }

    public Client(int id, String firstName, String lastName, String address, String phone, Location location, long clientNumber, ClientStatus status, Service service) {
        super(id, firstName, lastName, address, phone, location);
        this.clientNumber = clientNumber;
        this.status = status;
        this.service = service;
    }

    public long getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(long clientNumber) {
        this.clientNumber = clientNumber;
    }

    public ClientStatus getStatus() {
        return status;
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
