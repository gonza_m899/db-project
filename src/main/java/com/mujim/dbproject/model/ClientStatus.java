package com.mujim.dbproject.model;

/**
 * Created by gonzalo on 25/11/16.
 */
public enum ClientStatus {
    CORRECTO,
    NUEVO_PENDIENTE,
    SERVICIO_LIMITADO,
    SIN_SERVICIO,
    SIN_SERVICIO_REPORTADO,
    INACTIVO
}
