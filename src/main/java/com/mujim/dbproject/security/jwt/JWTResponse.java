package com.mujim.dbproject.security.jwt;

/**
 * Created by gonzalo on 23/11/16.
 */
public class JWTResponse {

    private String token;

    public JWTResponse() {
    }

    public JWTResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "{" + "\"token\": \"" + token + '\"' + "}";
    }
}
