Endpoints

/api/auth               POST            { username, password }
/api/clients            GET             -
/api/clients/create     POST            { client{...}, contract{...} }
/api/services           GET             -
/api/geo/locations      GET             -
/api/geo/zones          GET             -
/api/orders/complains   GET             -
/api/orders/roadmap     GET             zoneId
